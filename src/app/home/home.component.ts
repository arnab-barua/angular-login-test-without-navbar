import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  modalRef: BsModalRef;

  constructor(
    private toastrService : ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    
  }

  onSuccessClick(){
    this.toastrService.success("Success!!");
  }

  onErrorClick(){
    this.toastrService.error("Error!!");
  }

  onWarningClick() {
    this.toastrService.warning("Warning");
  }

  onInfoClick() {
    this.toastrService.info("Info!!");
  }

  onOpen(content) {
    this.modalRef = this.modalService.show(content);
  }





  

}
