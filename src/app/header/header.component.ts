import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})

export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;                  

  constructor(private authService: AuthService, private toastrService : ToastrService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn; 
  }

  onLogout(){
    this.toastrService.success("Logged Out", "", { timeOut : 10000});
    this.authService.logout();                      
  }

}
