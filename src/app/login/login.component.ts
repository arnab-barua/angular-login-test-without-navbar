import { User } from './../auth/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  
  private formSubmitAttempt: boolean; 
  user : User  = {
    userName : "",
    password : ""
  };
  
  constructor( private authService: AuthService ) {  }

  ngOnInit() { }
  
  onLogin() {
    this.authService.login(this.user);
  }
}
